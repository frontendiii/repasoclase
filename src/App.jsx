import { useState } from 'react';
import './App.css'
import Lista from './components/Lista'

function App() {
  const [buscar, setBuscar] = useState('');
  const [doctoresFiltrados, setDoctoresFiltrados] = useState([]);
  const doctores = [
    {
      id: 1,
      nombre: 'Juan',
      apellido: 'Perez',
      edad: 40,
      especialidad: 'Cirujano'
    },
    {
      id: 2,
      nombre: 'Maria',
      apellido: 'Gonzalez',
      edad: 35,
      especialidad: 'Pediatra'
    },
    {
      id: 3,
      nombre: 'Pedro',
      apellido: 'Gomez',
      edad: 45,
      especialidad: 'Cirujano'
    },
    {
      id: 4,
      nombre: 'Jose',
      apellido: 'Rodriguez',
      edad: 50,
      especialidad: 'Pediatra'
    },
    {
      id: 5,
      nombre: 'Carlos',
      apellido: 'Fernandez',
      edad: 55,
      especialidad: 'Cirujano'
    },
    {
      id: 6,
      nombre: 'Ana',
      apellido: 'Lopez',
      edad: 60,
      especialidad: 'Pediatra'
    },
    {
      id: 7,
      nombre: 'Laura',
      apellido: 'Diaz',
      edad: 65,
      especialidad: 'Cirujano'
    },
    {
      id: 8,
      nombre: 'Luis',
      apellido: 'Martinez',
      edad: 70,
      especialidad: 'Pediatra'
    },
    {
      id: 9,
      nombre: 'Jorge',
      apellido: 'Perez',
      edad: 75,
      especialidad: 'Cirujano'
    },
    {
      id: 10,
      nombre: 'Juan',
      apellido: 'Gonzalez',
      edad: 80,
      especialidad: 'Pediatra'
    }
  ]

  const handleSearch = () => {
    const filtrados = doctores.filter((doctor) => {
      return doctor.nombre.includes(buscar)
    })
    setDoctoresFiltrados(filtrados);
    console.log("Filtrados", filtrados);
  }

  const handleInputChange = (e) =>{
    setBuscar(e.target.value);
    handleSearch();
  }

  return (
    <>
      <input text="text" placeholder='Búsqueda' onChange={handleInputChange}/>
      <Lista data={doctoresFiltrados.length > 0 ? doctoresFiltrados : doctores} />
    </>
  )
}

export default App
