import React from 'react'

const Lista = (props) => {
    console.log(props.data);
  return (
    <React.Fragment>
        <div>Lista</div>
        <ul>
            {props.data.map((doctor) => {
                return (
                    <li key={doctor.id}>
                        {doctor.nombre} {doctor.apellido} {doctor.edad} {doctor.especialidad}
                    </li>
                )
            })}
        </ul>
    </React.Fragment>
  )
}

export default Lista